@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <!-- Header -->
            <div class="header mb-4">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-1">Overview</h6>
                            <h1 class="mb-0">Dashboard</h1>
                        </div>
                        <div class="col-auto">
                            {{-- <a href="" class="btn btn-primary">New Pick</a> --}}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Header -->

            <!-- Card -->
            <div class="card">
                <div class="card-body">
                    <p class="mb-0">This is the dashboard.</p>
                </div>
            </div>
            <!-- /Card -->
        </div>
    </div>
</div>
@endsection
