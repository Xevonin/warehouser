@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">
            <!-- Header -->
            <div class="header mb-4">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-1">Create</h6>
                            <h1 class="mb-0">Manufacturer</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Header -->

            <hr />

            <!-- Form -->
            <form method="post" action="{{ route('manufacturers.store') }}">
                @csrf
                <div class="form-group">
                    <label for="name">Manufacturer name</label>
                    <input type="text" class="form-control" name="name" id="name">
                </div>
                <div class="form-group">
                    <label for="url">Manufacturer website</label>
                    <input type="text" class="form-control" name="url" id="url">
                </div>
                <div class="form-group">
                    <label for="image" id="image-description">Manufacturer logo</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" aria-describedby="image-description">
                        <label class="custom-file-label" for="image">Choose file</label>
                    </div>
                </div>
                <hr />
                <button type="submit" class="btn btn-primary btn-block">Create Manufacturer</button>
            </form>
            <!-- /Form -->
        </div>
    </div>
</div>
@endsection