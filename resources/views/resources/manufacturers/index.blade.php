@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <!-- Header -->
            <div class="header mb-4">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-1">Overview</h6>
                            <h1 class="mb-0">Manufacturers</h1>
                        </div>
                        <div class="col-auto">
                            <a href="{{ route('manufacturers.create') }}" class="btn btn-primary">New Manufacturer</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Header -->

            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}  
                </div>
            @endif

            <!-- Card -->
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-nowrap card-table table-hover mb-0">
                        <thead>
                            <tr>
                                <th class="border-0" width="40%"><a href="#" class="text-muted sort">Name</a></th>
                                <th class="border-0" width="40%"><a href="#" class="text-muted sort">Website</a></th>
                                <th class="border-0"><a href="#" class="text-muted sort">Products</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($manufacturers as $manufacturer)
                                <tr>
                                    <td><a href="{{ route('manufacturers.show', $manufacturer->id)}}">{{ $manufacturer->name }}</a></td>
                                    <td><a href="{{ $manufacturer->url }}" target="blank">Website</a></td>
                                    <td>-</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /Card -->
        </div>
    </div>
</div>
@endsection