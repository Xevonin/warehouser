@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <!-- Header -->
            <div class="header mb-4">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-1">View Manufacturer</h6>
                            <h1 class="mb-0">{{ $manufacturer->name }}</h1>
                        </div>
                        <div class="col-auto">
                            <a href="{{ route('manufacturers.edit', $manufacturer->id) }}" class="btn btn-primary">Edit Manufacturer</a>
                        </div>
                    </div>
                    <div class="row align-items-center mt-2">
                        <div class="col">
                            <ul class="nav nav-tabs nav-overflow header-tabs">
                                <li class="nav-item">
                                    <a href="#" class="nav-link active">Products</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ $manufacturer->url }}" target="_blank" class="nav-link">Manufacturer Website</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Header -->

            <!-- Card -->
            <div class="card">
                @if ( ! empty( $manufacturer->products ) )
                    <div class="table-responsive">
                        <table class="table table-nowrap card-table table-hover mb-0">
                            <thead>
                                <tr>
                                    <th class="border-0"><a href="#" class="text-muted sort">Name</a></th>
                                    <th class="border-0"><a href="#" class="text-muted sort">Stock</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($manufacturer->products as $product)
                                    <tr>
                                        <td><a href="{{ route('products.show', $product->id) }}">{{ $product->name }}</a></td>
                                        <td>-</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                     <div class="card-body">
                        <p class="mb-0">No products have been added to this manufacturer.</p>
                     </div>
                @endif
            </div>
            <!-- /Card -->
        </div>
    </div>
</div>
@endsection