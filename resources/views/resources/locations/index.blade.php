@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <!-- Header -->
            <div class="header mb-4">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-1">Overview</h6>
                            <h1 class="mb-0">Locations</h1>
                        </div>
                        <div class="col-auto">
                            <a href="{{ route('locations.create') }}" class="btn btn-primary">New Location</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Header -->

            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}  
                </div>
            @endif

            <!-- Card -->
            <div class="card">
                {{-- <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <form class="input-group">
                                <input type="search" class="form-control form-control-flush" placeholder="Search Location" aria-label="Search Location" aria-describedby="button-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> --}}

                <div class="table-responsive">
                    <table class="table table-nowrap card-table table-hover mb-0">
                        <thead>
                            <tr>
                                <th class="border-0"><a href="#" class="text-muted sort">Name</a></th>
                                <th class="border-0"><a href="#" class="text-muted sort">Sub Locations</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($locations as $location)
                                <tr>
                                    <td><a href="{{ route('locations.show', $location->id)}}">{{ $location->name }}</a></td>
                                    <td>{{ $location->descendants->count() }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /Card -->
        </div>
    </div>
</div>
@endsection