@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">
            <!-- Header -->
            <div class="header mb-4">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-1">Create</h6>
                            <h1 class="mb-0">Location</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Header -->

            <hr />

            <!-- Form -->
            <form method="post" action="{{ route('locations.store') }}">
                @csrf
                <div class="form-group">
                    <label for="name">Location name</label>
                    <input type="text" class="form-control" name="name" id="name">
                </div>
                <div class="form-group">
                    <label for="parent_id">Location parent</label>
                    <select class="custom-select" name="parent_id" id="parent_id">
                        <option value="0" selected>No parent</option>
                        @foreach($locations as $location)
                            @if($parent == $location->id)
                                <option value="{{ $location->id }}" selected>{{ $location->name }}</option>
                            @else
                                <option value="{{ $location->id }}">{{ $location->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <hr />
                <button type="submit" class="btn btn-primary btn-block">Create Location</button>
            </form>
            <!-- /Form -->
        </div>
    </div>
</div>
@endsection