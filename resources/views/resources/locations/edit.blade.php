@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">
            <!-- Header -->
            <div class="header mb-4">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-1">Edit Location</h6>
                            <h1 class="mb-0">{{ $location->name }}</h1>
                        </div>
                        <form method="post" action="{{ route('locations.destroy', $location->id) }}" class="col-auto">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /Header -->

            <hr />

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <!-- Form -->
            <form method="post" action="{{ route('locations.update', $location->id) }}">
                @method('PATCH') 
                @csrf
                <div class="form-group">
                    <label for="name">Location name</label>
                    <input type="text" class="form-control" name="name" id="name" value="{{ $location->name }}">
                </div>
                <div class="form-group">
                    <label for="parent_id">Location parent</label>
                    <select class="custom-select" name="parent_id" id="parent_id">
                        <option value="0" selected>No parent</option>
                        @foreach($locations as $location_p)
                            @if($location->parent_id == $location_p->id)
                                <option value="{{ $location_p->id }}" selected>{{ $location_p->name }}</option>
                            @else
                                <option value="{{ $location_p->id }}">{{ $location_p->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <hr />
                <button type="submit" class="btn btn-primary btn-block">Update Location</button>
            </form>
            <!-- /Form -->
        </div>
    </div>
</div>
@endsection