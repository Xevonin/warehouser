@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <!-- Header -->
            <div class="header mb-4">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-1">View Location</h6>
                            <h1 class="mb-0">{{ $location->name }}</h1>
                        </div>
                        <div class="col-auto">
                            <a href="{{ route('locations.create', ['parent' => $location->id]) }}" class="btn btn-primary">New Sub Location</a>
                            <a href="{{ route('locations.edit', $location->id) }}" class="btn btn-secondary">Edit Location</a>
                        </div>
                    </div>
                    <div class="row align-items-center mt-2">
                        <div class="col">
                            <ul class="nav nav-tabs nav-overflow header-tabs">
                                <li class="nav-item">
                                    <a href="#" class="nav-link active">Sub Locations</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">Stock</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Header -->

            <!-- Card -->
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-nowrap card-table table-hover mb-0">
                        <thead>
                            <tr>
                                <th class="border-0"><a href="#" class="text-muted sort">Name</a></th>
                                <th class="border-0"><a href="#" class="text-muted sort">Sub Locations</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sublocations as $sublocation)
                                <tr>
                                    <td><a href="{{ route('locations.show', $sublocation->id)}}">{{ $sublocation->name }}</a></td>
                                    <td>{{ $sublocation->descendants->count() }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /Card -->
        </div>
    </div>
</div>
@endsection