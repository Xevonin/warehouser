@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">
            <!-- Header -->
            <div class="header mb-4">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-1">Create</h6>
                            <h1 class="mb-0">Product</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Header -->

            <hr />

            <!-- Form -->
            <form method="post" action="{{ route('products.store') }}">
                @csrf
                <div class="form-group">
                    <label for="name">Product name</label>
                    <input type="text" class="form-control" name="name" id="name">
                </div>
                <div class="form-group">
                    <label for="manufacturer_id">Product manufacturer</label>
                    <select class="custom-select" name="manufacturer_id" id="manufacturer_id">
                        @foreach($manufacturers as $manufacturer)
                            <option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="gtin">Product GTIN</label>
                    <input type="text" class="form-control" name="gtin" id="gtin">
                </div>
                <div class="form-group">
                    <label for="weight">Product Weight</label>
                    <input type="text" class="form-control" name="weight" id="weight">
                </div>
                <hr />
                <button type="submit" class="btn btn-primary btn-block">Create Product</button>
            </form>
            <!-- /Form -->
        </div>
    </div>
</div>
@endsection