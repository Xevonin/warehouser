@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <!-- Header -->
            <div class="header mb-4">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-1">View Product</h6>
                            <h1 class="mb-0">{{ $product->name }}</h1>
                        </div>
                        <div class="col-auto">
                            <a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary">Edit Product</a>
                        </div>
                    </div>
                    <div class="row align-items-center mt-2">
                        <div class="col">
                            <ul class="nav nav-tabs nav-overflow header-tabs">
                                <li class="nav-item">
                                    <a href="#" class="nav-link active">Stock</a>
                                </li>
                                @if (!empty($product->manufacturer))
                                    <li class="nav-item">
                                        <a href="{{ route('manufacturers.show', $product->manufacturer->id) }}" class="nav-link">Manufacturer</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Header -->

            <!-- Card -->
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-nowrap card-table table-hover mb-0">
                        <thead>
                            <tr>
                                <th class="border-0"><a href="#" class="text-muted sort">Location</a></th>
                                <th class="border-0"><a href="#" class="text-muted sort">Stock</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /Card -->
        </div>
    </div>
</div>
@endsection