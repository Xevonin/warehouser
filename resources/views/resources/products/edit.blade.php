@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">
            <!-- Header -->
            <div class="header mb-4">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-1">Edit Product</h6>
                            <h1 class="mb-0">{{ $product->name }}</h1>
                        </div>
                        <form method="post" action="{{ route('products.destroy', $product->id) }}" class="col-auto">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /Header -->

            <hr />

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <!-- Form -->
            <form method="post" action="{{ route('products.update', $product->id) }}">
                @method('PATCH') 
                @csrf
                <div class="form-group">
                    <label for="name">Product name</label>
                    <input type="text" class="form-control" name="name" id="name" value="{{ $product->name }}">
                </div>
                <div class="form-group">
                    <label for="manufacturer_id">Product manufacturer</label>
                    <select class="custom-select" name="manufacturer_id" id="manufacturer_id">
                        @foreach($manufacturers as $manufacturer)
                            @if($manufacturer->id == $product->manufacturer_id)
                                <option value="{{ $manufacturer->id }}" selected>{{ $manufacturer->name }}</option>
                            @else
                                <option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="gtin">Product GTIN</label>
                    <input type="text" class="form-control" name="gtin" id="gtin" value="{{ $product->gtin }}">
                </div>
                <div class="form-group">
                    <label for="weight">Product Weight</label>
                    <input type="text" class="form-control" name="weight" id="weight" value="{{ $product->weight }}">
                </div>
                <hr />
                <button type="submit" class="btn btn-primary btn-block">Update Product</button>
            </form>
            <!-- /Form -->
        </div>
    </div>
</div>
@endsection