<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'url', 
        'image'
    ];

    public function products()
    {
        return $this->hasMany('App\Product', 'manufacturer_id', 'id');
    }
}
