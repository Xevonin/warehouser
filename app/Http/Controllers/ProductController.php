<?php

namespace App\Http\Controllers;

use App\Product;
use App\Manufacturer;
use App\Http\Requests\StoreProduct;
use App\Http\Requests\UpdateProduct;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Return all products.
        $products = Product::all(); 

        // Show the products index.
        return view('resources.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Retrieve all the manufacturers for the dropdown.
        // TODO: Probably have to replace this with an ajax search later on.
        $manufacturers = Manufacturer::orderBy('name')->get(); 

        // Show the product creation form.
        return view('resources.products.create', compact('manufacturers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProduct $request)
    {
        // Retrieve the validated data from StoreProduct.
        $validated = $request->validated();

        // Create the product with the validated data.
        Product::create($validated);
        
        // Return to the products index with a success message.
        return redirect()->route('products.index')->with('success', 'Product is successfully saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        // Show the product.
        return view('resources.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        // Retrieve all the manufacturers for the dropdown.
        $manufacturers = Manufacturer::orderBy('name')->get(); 

        // Show the product edit form.
        return view('resources.products.edit', compact('product', 'manufacturers'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProduct $request, Product $product)
    {
        // Retrieve the validated data from UpdateProduct.
        $validated = $request->validated();

        // Update the product with the validated data.
        Product::whereId($product->id)->update($validated);
        
        // Return to the products index with a success message.
        return redirect()->route('products.index')->with('success', 'Product is successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        // Delete the product.
        $product->delete();

        // Return to the products index with a success message.
        return redirect()->route('products.index')->with('success', 'Product is successfully deleted!');
    }
}
