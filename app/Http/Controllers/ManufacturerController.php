<?php

namespace App\Http\Controllers;

use App\Manufacturer;
use App\Http\Requests\StoreManufacturer;
use App\Http\Requests\UpdateManufacturer;
use Illuminate\Http\Request;

class ManufacturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Return all manufacturers.
        $manufacturers = Manufacturer::orderBy('name')->get(); 

        // Show the manufacturers index.
        return view('resources.manufacturers.index', compact('manufacturers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Show the manufacturer creation form.
        return view('resources.manufacturers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreManufacturer $request)
    {
        // Retrieve the validated data from StoreManufacturer.
        $validated = $request->validated();

        // Create the manufacturer with the validated data.
        Manufacturer::create($validated);
        
        // Return to the manufacturers index with a success message.
        return redirect()->route('manufacturers.index')->with('success', 'Manufacturer is successfully saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Manufacturer  $manufacturer
     * @return \Illuminate\Http\Response
     */
    public function show(Manufacturer $manufacturer)
    {
        // Show the manufacturer.
        return view('resources.manufacturers.show', compact('manufacturer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Manufacturer  $manufacturer
     * @return \Illuminate\Http\Response
     */
    public function edit(Manufacturer $manufacturer)
    {
        // Show the manufacturer edit form.
        return view('resources.manufacturers.edit', compact('manufacturer'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Manufacturer  $manufacturer
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateManufacturer $request, Manufacturer $manufacturer)
    {
        // Retrieve the validated data from UpdateManufacturer.
        $validated = $request->validated();

        // Update the manufacturer with the validated data.
        Manufacturer::whereId($manufacturer->id)->update($validated);

        // Return to the manufacturers index with a success message.
        return redirect()->route('manufacturers.index')->with('success', 'Manufacturer is successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Manufacturer  $manufacturer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Manufacturer $manufacturer)
    {
        // Delete the manufacturer.
        $manufacturer->delete();

        // Return to the manufacturers index with a success message.
        return redirect()->route('manufacturers.index')->with('success', 'Manufacturer is successfully deleted!');
    }
}
