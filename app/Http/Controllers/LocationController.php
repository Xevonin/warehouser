<?php

namespace App\Http\Controllers;

use App\Location;
use App\Http\Requests\StoreLocation;
use App\Http\Requests\UpdateLocation;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Only return root level locations on the index page.
        $locations = Location::whereIsRoot()->get();

        // Show the locations index.
        return view('resources.locations.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // Retrieve all the locations for the parent dropdown.
        // TODO: Probably have to replace this with an ajax search later on.
        $locations = Location::orderBy('name')->get(); 

        // Check if there is a parent defined in the request.
        $parent = $request->get('parent', false);

        // Show the location creation form.
        return view('resources.locations.create', compact('locations', 'parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLocation $request)
    {
        // Retrieve the validated data from StoreLocation.
        $validated = $request->validated();

        // Create the location with the validated data.
        $location = Location::create($validated);
        
        // Seperate the parent ID from the request.
        $parent = $request->get('parent_id');
        
        // If there is a parent it needs to be added as a node, otherwise it needs to be saved as a root.
        if($parent > 0) {
            $parent = Location::findOrFail($parent);
            $location->appendToNode($parent)->save();
        } else {
            $location->saveAsRoot();
        }

        // Return to the locations index with a success message.
        return redirect()->route('locations.index')->with('success', 'Location is successfully saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        // Retrieve all the descendants of the shown location.
        $sublocations = Location::whereDescendantOf($location)->where('parent_id', '=', $location->id)->get();

        // Show the location.
        return view('resources.locations.show', compact('location', 'sublocations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        // Retrieve all the locations for the parent dropdown.
        $locations = Location::orderBy('name')->get();

        // Show the location edit form.
        return view('resources.locations.edit', compact('location', 'locations'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLocation $request, Location $location)
    {
        // Retrieve the validated data from UpdateLocation.
        $validated = $request->validated();
        
        // Update the location with the validated data.
        Location::whereId($location->id)->update($validated);

        // Seperate the parent ID from the request.
        $parent = $request->get('parent_id');

        // If there is a parent it needs to be added as a node, otherwise it needs to be saved as a root.
        if($parent > 0) {
            $parent = Location::findOrFail($parent);
            $location->appendToNode($parent)->save();
        } else {
            $location->saveAsRoot();
        }
        
        // Return to the locations index with a success message.
        return redirect()->route('locations.index')->with('success', 'Location is successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        // Delete the location.
        $location->delete();

        // Return to the locations index with a success message.
        return redirect()->route('locations.index')->with('success', 'Location is successfully deleted!');
    }
}
