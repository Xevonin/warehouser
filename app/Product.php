<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'gtin', 
        'weight',
        'manufacturer_id'
    ];

    public function manufacturer()
    {
        return $this->belongsTo('App\Manufacturer', 'manufacturer_id', 'id');
    }
}
